#pragma once

// Note
// The string must be same as the string in CSCFeatureTagCommon.java
// Because one feature coulde be implemented both in java layer and in native layer
#define TAG_CSCFEATURE_COMMON_USECHAMELEON "CscFeature_Common_UseChameleon"
