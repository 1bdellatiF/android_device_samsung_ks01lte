#pragma once

// Note
// The string must be same as the string in CSCFeatureTagFramework.java
// Because one feature coulde be implemented both in java layer and in native layer


#define TAG_CSCFEATURE_FRAMEWORK_ENABLEBIDIRECTION       "CscFeature_Framework_EnableBidirection"

#define TAG_CSCFEATURE_FRAMEWORK_ENABLEHARFBUZZ          "CscFeature_Framework_EnableHarfbuzz"

#define TAG_CSCFEATURE_FRAMEWORK_ENABLETHAIVIETRESHAPING "CscFeature_Framework_EnableThaiVietReshaping"
